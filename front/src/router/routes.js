import Login from "pages/Login";
import IndexPage from "pages/IndexPage";
import MisDatos from "pages/MisDatos";
import Mascota from "pages/Mascota";
import Solicitudes from "pages/Solicitudes";
import Verificacion from "pages/Verificacion";
import Impresion from "pages/Impresion";
import Reporte from "pages/Reporte";

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component:IndexPage,meta: { requiresAuth: true } },
      { path: 'misdatos', component:MisDatos,meta: { requiresAuth: true } },
      { path: 'mascota', component:Mascota,meta: { requiresAuth: true } },
      { path: 'verificacion', component:Verificacion,meta: { requiresAuth: true } },
       { path: 'impresion', component:Impresion,meta: { requiresAuth: true } },
      { path: 'reporte', component:Reporte,meta: { requiresAuth: true } },
      // { path: 'solicitudes', component:IndexPage,meta: { requiresAuth: true } },
    ]
  },
  { path: '/login', component: Login },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
