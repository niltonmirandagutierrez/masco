<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/login', [\App\Http\Controllers\UserController::class, 'login']);
Route::post('/loginExterior', [\App\Http\Controllers\UserController::class, 'loginExterior']);
//Route::get('credencialFile', [\App\Http\Controllers\CupoController::class, 'credencialFile']);
Route::get('buscarMascota/{chip}', [\App\Http\Controllers\MascotaController::class, 'buscarMascota']);
Route::get('buscarUser/{ci}', [\App\Http\Controllers\UserController::class, 'buscarUser']);
Route::post('validar', [\App\Http\Controllers\PedidoController::class, 'validar']);
Route::apiResource('destino', \App\Http\Controllers\DestinoController::class);
Route::post('upload', [\App\Http\Controllers\UploadController::class, 'upload']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::apiResource('pedido', \App\Http\Controllers\PedidoController::class);
    Route::post('/logout', [\App\Http\Controllers\UserController::class, 'logout']);
    Route::post('/listPendiente/{ciudad}', [\App\Http\Controllers\PedidoController::class, 'listPendiente']);
    Route::post('/listAprobado/{ciudad}', [\App\Http\Controllers\PedidoController::class, 'listAprobado']);
    Route::post('/aprobar', [\App\Http\Controllers\PedidoController::class, 'aprobar']);
    Route::post('/observar', [\App\Http\Controllers\PedidoController::class, 'observar']);
    Route::post('/me', [\App\Http\Controllers\UserController::class, 'me']);
    Route::post('/report', [\App\Http\Controllers\ReportController::class, 'report']);
    Route::post('/anular', [\App\Http\Controllers\PedidoController::class, 'anular']);
    Route::post('/rectificar', [\App\Http\Controllers\PedidoController::class, 'rectificar']);
    Route::post('/reportPagos', [\App\Http\Controllers\ReportController::class, 'reportPagos']);
    Route::apiResource('user', \App\Http\Controllers\UserController::class);
    Route::apiResource('mascota', \App\Http\Controllers\MascotaController::class);
});
