<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function report(Request $request)
    {
        $validated = $request->validate([
            'fechaDesde' => 'required',
            'fechaHasta' => 'required',
            'type' => 'required',
        ]);

        $fechaDesde = $request->input('fechaDesde');
        $fechaHasta = $request->input('fechaHasta');
        $tipoReporte = $request->input('type');
        if ($tipoReporte=='economicoPago'){//// tipo pago
            return DB::select("SELECT pa.forma, count(*) cantidad
            FROM pedidos pe
            INNER JOIN pagos pa ON pa.pedido_id=pe.id
            WHERE estado='aprobado'
            AND pe.emision>='$fechaDesde'
            AND pe.emision<='$fechaHasta'
            GROUP BY pa.forma;");
        }elseif ($tipoReporte=='economicoMascota'){////  mascota deestino
            return DB::select("SELECT d.nombre, count(*) cantidad
            FROM pedidos pe
            INNER JOIN destinos d ON pe.destino_id=d.id
            WHERE estado='aprobado'
            AND pe.emision>='$fechaDesde'
            AND pe.emision<='$fechaHasta'
            GROUP BY d.nombre;");
        }elseif ($tipoReporte=='emision'){////  canino felino
            return DB::select("SELECT m.tipo, count(*) cantidad
            FROM pedidos pe
            INNER JOIN mascotas m ON pe.mascota_id=m.id
            WHERE estado='aprobado'
            AND pe.emision>='$fechaDesde'
            AND pe.emision<='$fechaHasta'
            GROUP BY m.tipo;");
        }elseif ($tipoReporte=='anulados'){ /// total anulado
            return DB::SELECT("SELECT p.*,m.tipo,m.nombre mascota,a.talonario,a.fecha,a.motivo,a.codigo certificado
            from anulados a inner join pedidos p on a.pedido_id=p.id
            inner join mascotas m on p.mascota_id=m.id
            where
              fecha>='$fechaDesde'
            AND fecha<='$fechaHasta'");
        }elseif($tipoReporte=='listado'){ // dato
            return DB::SELECT("SELECT p.* exportador,m.tipo,m.nombre mascota,p.origen,d.nombre destino,
            (SELECT sum(g.total) from pagos g where g.pedido_id=p.id) monto,
            (SELECT sum(g.diff) from pagos g where g.pedido_id=p.id) diff
            from pedidos p inner join mascotas m ON m.id = p.mascota_id
            INNER join destinos d ON d.id = p.destino_id
            where p.emision>='$fechaDesde'
            AND p.emision<='$fechaHasta' ");
        }
    }

    public function reportPagos(Request $request){
       // return $request;
        if($request->origen==''){
            return DB::SELECT("SELECT e.talonario,pa.solicitante,pa.fecha
            ,pa.transaccion,pa.total,pa.nroliquidacion from pedidos e inner join pagos pa on pa.pedido_id = e.id
            where date(pa.fecha)>='$request->fechaDesde'
            and date(pa.fecha)<='$request->fechaHasta'
            and pa.servicio='$request->servicio'");
        }
        else{
            return DB::SELECT("SELECT e.talonario,pa.solicitante,pa.fecha
            ,pa.transaccion,pa.total,pa.nroliquidacion from pedidos e inner join pagos pa on pa.pedido_id = e.id
            where date(pa.fecha)>='$request->fechaDesde'
            and date(pa.fecha)<='$request->fechaHasta'
            and e.origen=$request->origen
            and pa.servicio='$request->servicio'");
        }
    }
}
