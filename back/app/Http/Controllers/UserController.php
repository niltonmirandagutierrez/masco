<?php

namespace App\Http\Controllers;

use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function login(Request $request){
        $request->validate([
            'tipoParametro' => 'required',
            'valor' => 'required'
        ]);

        $postData = array('nombreUsuario'=>$request->tipoParametro, 'claveUsuario'=>$request->valor);

        $data_string=json_encode($postData);
        $autorization="JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb3VudHJ5IjoiQm9saXZpYSIsIm5hbWUiOiJQYXNjdWFsIE1pcmFuZGEiLCJlbWFpbCI6InBtaXJhbmRhQHNlbmFzYWcuZ29iLmJvIn0=.2nH7e0QFUtp6FATN0s+xkFSGoJ/uBpsmD8ayDoNQSaQ=";
        $headers = array(
            'Content-type: application/json',
            'Authorization: '.trim($autorization),
            'Content-Length: ' . strlen($data_string),
            //'cache-control: no-cache',
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://test.senasag.gob.bo/wspaititi/api/sia/funcionario/autentificar");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

        //curl_setopt($ch, CURLOPT_HEADER, true);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
            exit();
        }
        //return json_decode($data);
        if ($data!='' ) {
            $data = json_decode($data);
            if ($data->finalizado && $data->datos!='' ) {
                $user = User::where('email', $data->datos->correo)->first();
                $rol=$this->validar2(['distrital'=>$data->datos->distrital,'nroci'=>$data->datos->nroci]);
                if ($user) {
                    $user->apellidos = $data->datos->apellidoMaterno.' '.$data->datos->apellidoPaterno;
                    $user->name = $data->datos->nombres;
                    $user->tipo = $rol;
                    $user->ciudad= $data->datos->distrital;
                    $user->save();
                    $token = $user->createToken('authToken')->plainTextToken;
                    return response(['user' => $user, 'token' => $token]);
                } else {
                    $user = User::create([
                        'apellidos' => $data->datos->apellidoMaterno.' '.$data->datos->apellidoPaterno,
                        'name' => $data->datos->nombres,
                        'email' => $data->datos->correo,
                        'ci' => $data->datos->nroci,
                        'tipo' => $rol,
                        'ciudad'=> $data->datos->distrital,
                        'password' => Hash::make('12345678')
                    ]);
                    $token = $user->createToken('authToken')->plainTextToken;
                    return response(['user' => User::find($user->id), 'token' => $token]);
                }
            } else {
                return response(['message' => 'Usuario no encontrado'],500);
            }
        }else{
            return response(['message' => 'Usuario no encontrado'],500);
        }
    }

    public function validar2($user){


        $postData = array('identificador'=>'ci', 'valorIdentificador'=>$user['nroci']
        ,'codigoDeptal'=>$user['distrital'],'codigoServicio'=>'CZEM');

        $data_string=json_encode($postData);
        $autorization="JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb3VudHJ5IjoiQm9saXZpYSIsIm5hbWUiOiJQYXNjdWFsIE1pcmFuZGEiLCJlbWFpbCI6InBtaXJhbmRhQHNlbmFzYWcuZ29iLmJvIn0=.GP0wN8yB7/5G2g5UuAmtGklhqGWlvcsny12zak2ryaM=";
        $headers = array(
            'Content-type: application/json',
            'Authorization: '.trim($autorization),
            'Content-Length: ' . strlen($data_string),
            //'cache-control: no-cache',
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://test.senasag.gob.bo/wspaititi/api/pservicios/admin/usuario/verificar");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

        //curl_setopt($ch, CURLOPT_HEADER, true);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            return '';
        }
        //return json_decode($data);
        if ($data!='') {
            $data = json_decode($data);
            if ($data->finalizado) {
                    return $data->datos->idRol;
                }
            } else {
                return '';
        }
    }


    public function loginExterior(Request $request){
        $request->validate([
            'ci' => 'required',
        ]);

        $user = User::where('ci', $request->ci)->first();

        if (! $user) {
            $user=new User();
            $user->ci=$request->ci;
            $user->save();
            return response()->json([
                'token'=>$user->createToken('web')->plainTextToken,
                'user'=>User::find($user->id)
            ],200);
        }
        return response()->json([
            'token'=>$user->createToken('web')->plainTextToken,
            'user'=>$user
        ],200);
    }

    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();
        return response()->json(['res'=>'salido exitosamente'],200);
    }
    public function me(Request $request){
        $user=User::where('id',$request->user()->id)->firstOrFail();
        return $user;
    }
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function buscarUser($ci){
        return User::where('ci',$ci)->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        return $user->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
