<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Models\User;
use App\Models\Pago;
use App\Models\Anulado;
use App\Http\Requests\StorePedidoRequest;
use App\Http\Requests\UpdatePedidoRequest;
use Illuminate\Http\Request;

class PedidoController extends Controller
{
    public function index(Request $request){
        if ($request->user()->tipo == 'IT' || $request->user()->tipo == 'ET') {
            return Pedido::with('destino')->with('pagos')->with('mascota')->whereOrigen( $request->user()->ciudad)->get();
        } else {
            return Pedido::with('destino')->with('pagos')->with('mascota')->where('user_id', $request->user()->id)->get();
        }

    }

    public function listPendiente($ciudad){
        return Pedido::with('destino')->with('mascota')->where('verificacion','pendiente')->where('origen',$ciudad)->get();
    }

    public function listAprobado(Request $request){
        return Pedido::with('destino')->with('mascota')->with('pagos')->where('estado','aprobado')->get();
    }

    public function show(Pedido $pedido){ return $pedido; }
    public function store(Request $request){
        /*$num=Pedido::where('origen',$request->origen)->get();
        if(sizeof($num)==0)
            $codigo=750;
        else{
            $numero=Pedido::where('origen',$request->origen)->orderBy('codigo', 'desc')->first();
            $codigo=intval($numero->codigo)+1;
        }

        $ntalon=Pedido::all();
        if(sizeof($ntalon)==0)
            $talonario=50000;
        else{
            $ntalon=Pedido::orderBy('talonario','desc')->first();
            $talonario=intval($ntalon->talonario) + 1;
        }*/

        $pedido=new Pedido();
        $pedido->nombreExportador=$request->nombreExportador;
        $pedido->documentoExportador=$request->documentoExportador;
        $pedido->direccionExportador=$request->direccionExportador;
        $pedido->celularExportador=$request->celularExportador;
        $pedido->correoExportador=$request->correoExportador;
        $pedido->codigo=0;
        $pedido->documentoImportador=$request->documentoImportador;
        $pedido->nombreImportador=$request->nombreImportador;
        $pedido->direccionImportador=$request->direccionImportador;
        $pedido->celularImportador=$request->celularImportador;
        $pedido->correoImportador=$request->correoImportador;
        $pedido->importe=$request->importe;
        $pedido->nroliquidacion=$request->nroliquidacion;
        $pedido->fechapago=$request->fechapago;
        $pedido->origen=$request->origen;
        $pedido->veterinario=$request->veterinario;
        $pedido->descripcion=$request->descripcion;
        $pedido->fechaRevision=$request->fechaRevision;
        $pedido->veterinario2=$request->veterinario2;
        $pedido->descripcion2=$request->descripcion2;
        $pedido->fechaRevision2=$request->fechaRevision2;
        $pedido->veterinario3=$request->veterinario3;
        $pedido->descripcion3=$request->descripcion3;
        $pedido->fechaRevision3=$request->fechaRevision3;
        $pedido->documento=$request->documento;
        $pedido->documento2=$request->documento2;
        $pedido->documento3=$request->documento3;
        $pedido->transporte=$request->transporte;
        $pedido->lugarLlegada=$request->lugarLlegada;
        $pedido->puertoSalida=$request->puertoSalida;
        $pedido->inicio=date('Y-m-d');
        $pedido->destino_id=$request->destino_id;
        $pedido->user_id=$request->user()->id;
        $pedido->mascota_id=$request->mascota_id;
        $pedido->talonario=0;
        $pedido->save();
        $liq=New Pago();
        $liq->nroliquidacion=$request->liquidacion['nroliquidacion'];
        $liq->servicio=$request->liquidacion['servicio'];
        $liq->nombre=$request->liquidacion['nombre'];
        $liq->solicitante=$request->liquidacion['solicitante'];
        $liq->costo=$request->liquidacion['costo'];
        $liq->total=$request->liquidacion['total'];
        $liq->diff=$request->liquidacion['diff'];
        $liq->codigo=$request->liquidacion['codigo'];
        $liq->fecha=$request->liquidacion['fecha'];
        $liq->transaccion=$request->liquidacion['transaccion'];
        $liq->forma=$request->liquidacion['forma'];
        $liq->pedido_id=$pedido->id;
        $liq->save();
        $servicio='154';
        $this->consumirPago(['nroliquidacion'=>$pedido->nroliquidacion,'id'=>$pedido->origen.'-'.$pedido->codigo,'user'=>$request->user()->name.' '.$request->user()->apellidos,'servicio'=>$servicio]);
        return true;
    }


    public function update(Request $request, Pedido $pedido){
        if($request->verificacion=='AMPLIAR'){
            $pedido=Pedido::find($request->id);
            $pedido->estado='pendiente';
            $pedido->save();

            $liq=New Pago();
            $liq->nroliquidacion=$request->liquidacion['nroliquidacion'];
            $liq->servicio=$request->liquidacion['servicio'];
            $liq->nombre=$request->liquidacion['nombre'];
            $liq->solicitante=$request->liquidacion['solicitante'];
            $liq->costo=$request->liquidacion['costo'];
            $liq->total=$request->liquidacion['total'];
            $liq->diff=$request->liquidacion['diff'];
            $liq->codigo=$request->liquidacion['codigo'];
            $liq->fecha=$request->liquidacion['fecha'];
            $liq->transaccion=$request->liquidacion['transaccion'];
            $liq->forma=$request->liquidacion['forma'];
            $liq->pedido_id=$pedido->id;
            $liq->save();
            $servicio='217';
            $this->consumirPago(['nroliquidacion'=>$pedido->nroliquidacion,'id'=>$pedido->origen.'-'.$pedido->codigo,'user'=>$request->user()->name.' '.$request->user()->apellidos,'servicio'=>$servicio]);            
            return true;
        }

        $pedido=Pedido::find($request->id);
        $pedido->nombreExportador=$request->nombreExportador;
        $pedido->documentoExportador=$request->documentoExportador;
        $pedido->direccionExportador=$request->direccionExportador;
        $pedido->celularExportador=$request->celularExportador;
        $pedido->correoExportador=$request->correoExportador;
        $pedido->documentoImportador=$request->documentoImportador;
        $pedido->nombreImportador=$request->nombreImportador;
        $pedido->direccionImportador=$request->direccionImportador;
        $pedido->celularImportador=$request->celularImportador;
        $pedido->correoImportador=$request->correoImportador;
        //$pedido->importe=$request->importe;
        //$pedido->nroliquidacion=$request->nroliquidacion;
        //$pedido->fechapago=$request->fechapago;
        //$pedido->origen=$request->origen;
        $pedido->veterinario=$request->veterinario;
        $pedido->descripcion=$request->descripcion;
        $pedido->fechaRevision=$request->fechaRevision;
        $pedido->veterinario2=$request->veterinario2;
        $pedido->descripcion2=$request->descripcion2;
        $pedido->fechaRevision2=$request->fechaRevision2;
        $pedido->veterinario3=$request->veterinario3;
        $pedido->descripcion3=$request->descripcion3;
        $pedido->fechaRevision3=$request->fechaRevision3;
        $pedido->documento=$request->documento;
        $pedido->documento2=$request->documento2;
        $pedido->documento3=$request->documento3;
        $pedido->transporte=$request->transporte;
        $pedido->lugarLlegada=$request->lugarLlegada;
        $pedido->puertoSalida=$request->puertoSalida;
        $pedido->destino_id=$request->destino_id;
        $pedido->mascota_id=$request->mascota_id;
        $pedido->estado='pendiente';
        //$pedido->verificacion='pendiente';
        if($pedido->verificacion!='PEDIDO'){
            $pedido->save();
        }
        if($request->verificacion=='PEDIDO'){
            $liq=New Pago();
            $liq->nroliquidacion=$request->liquidacion['nroliquidacion'];
            $liq->servicio=$request->liquidacion['servicio'];
            $liq->nombre=$request->liquidacion['nombre'];
            $liq->solicitante=$request->liquidacion['solicitante'];
            $liq->costo=$request->liquidacion['costo'];
            $liq->total=$request->liquidacion['total'];
            $liq->diff=$request->liquidacion['diff'];
            $liq->codigo=$request->liquidacion['codigo'];
            $liq->fecha=$request->liquidacion['fecha'];
            $liq->transaccion=$request->liquidacion['transaccion'];
            $liq->forma=$request->liquidacion['forma'];
            $liq->pedido_id=$pedido->id;
            $liq->save();
            $servicio='222';
            $this->consumirPago(['nroliquidacion'=>$pedido->nroliquidacion,'id'=>$pedido->origen.'-'.$pedido->codigo,'user'=>$request->user()->name.' '.$request->user()->apellidos,'servicio'=>$servicio]);            
        }

        return true;
       // return $pedido;
     }

    public function destroy(Pedido $pedido){ $pedido->delete(); return $pedido; }

    public function consumirPago($datos){
        //$servicio='154';
        $postData = array('nroLiquidacion'=>$datos['nroliquidacion'],'codigoServicio'=>$datos['servicio'],'tabla'=>'pedido',
        'idRegistro'=>$datos['id'],
        'direccionIp'=>$_SERVER['REMOTE_ADDR'],
        'nombreUsuario'=>$datos['user']);

        $data_string=json_encode($postData);
        $autorization="JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb3VudHJ5IjoiQm9saXZpYSIsIm5hbWUiOiJNaXJpYW4gR2FybmljYSIsImVtYWlsIjoibWdhcm5pY2FAc2VuYXNhZy5nb2IuYm8ifQ==.nQV9h2KlefqysEeCCwsBT+EmVfrJ2zZvpf4NtEoT3po=";
        $headers = array(
            'Content-type: application/json',
            'Authorization: '.trim($autorization),
            'Content-Length: ' . strlen($data_string),
            //'cache-control: no-cache',
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://test.senasag.gob.bo/wspaititi/api/liquidador/liquidacion/consumirServicio");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

        //curl_setopt($ch, CURLOPT_HEADER, true);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
            exit();
        }
        error_log($data);
//        return json_decode($data);
        if ($data!='') {
            $data = json_decode($data);
            if ($data->finalizado) {
                return $data;
                }
             else {
                return response(['message' => 'No Existe liquidacion'],500);
            }
        }else{
            return response(['message' => 'No Existe liquidacion'],500);
        }
    }

    public function aprobar(Request $request){
        $num=Pedido::where('origen',$request->origen)->get();
        if(sizeof($num)==0)
            $codigo=750;
        else{
            $numero=Pedido::where('origen',$request->origen)->orderBy('codigo', 'desc')->first();
            $codigo=intval($numero->codigo) + 1;
        }

        $ntalon=Pedido::all();
        if(sizeof($ntalon)==0)
            $talonario=50000;
        else{
            $ntalon=Pedido::orderBy('talonario','desc')->first();
            $talonario=intval($ntalon->talonario) + 1;
        }
        $pedido=Pedido::find($request->id);
        //return $request;
        $pedido->talonario=$talonario;
        $pedido->observacion=$request->observacion;
        $pedido->estado='aprobado';
        //$pedido->verificacion='aprobado';
        if( $pedido->verificacion!='AMPLIAR'){
            $pedido->validez=date('Y-m-d', strtotime($pedido->emision." +60 days"));
            
        }
        if($pedido->verificacion!='PEDIDO' && $pedido->verificacion!='AMPLIAR' && $pedido->verificacion!='TECNICO'){
            $pedido->validez=date('Y-m-d', strtotime("+30 days"));
            $pedido->emision=date('Y-m-d');
            $pedido->codigo=$codigo;
        }
        $pedido->senasag_id=$request->user()->id;
        $pedido->save();
    }

    public function observar(Request $request){
        $pedido=Pedido::find($request->id);
        $pedido->emision=date('Y-m-d');
        $pedido->observacion=$request->observacion;
        $pedido->estado='observado';
        $pedido->verificacion='observado';
        $pedido->senasag_id=$request->user()->id;
        $pedido->save();
    }

    public function anular(Request $request){
        $pedido= Pedido::find($request->id);
        $anulado=new Anulado();
        $anulado->talonario=$pedido->talonario;
        $anulado->codigo=$pedido->origen.'-'.$pedido->codigo;
        $anulado->fecha=date('Y-m-d');
        $anulado->hora=date('H:i:s');
        $anulado->pedido_id=$pedido->id;
        $anulado->motivo=$request->motivo;
        $anulado->user_id=$request->user()->id;
        $anulado->tipo='ANULADO';
        $anulado->save();
        $pedido->estado='anulado';
        $pedido->verificacion='anulado';
        $pedido->save();
    }

    public function rectificar(Request $request){
        $pedido= Pedido::find($request->id);
        $anulado=new Anulado();
        $anulado->talonario=$pedido->talonario;
        $anulado->codigo=$pedido->origen.'-'.$pedido->codigo;
        $anulado->fecha=date('Y-m-d');
        $anulado->hora=date('H:i:s');
        $anulado->pedido_id=$pedido->id;
        $anulado->motivo=$request->motivo;
        $anulado->tipo='RECTIFICAR ' . $request->tipo;
        $anulado->user_id=$request->user()->id;
        $anulado->save();

        $pedido->estado='rectificar';
        $pedido->verificacion=$request->tipo;
         if($request->tipo=='AMPLIAR'){
            $pedido->validez=date('Y-m-d', strtotime($pedido->validez."+30 days"));
        } 
        $pedido->save();
    }

    public function validar(Request $request){
        //return $request;
        //$servicio='154';
        $postData = array('nroLiquidacion'=>$request->nroliquidacion,'codigoServicio'=>$request->servicio);

        $data_string=json_encode($postData);
        $autorization="JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb3VudHJ5IjoiQm9saXZpYSIsIm5hbWUiOiJNaXJpYW4gR2FybmljYSIsImVtYWlsIjoibWdhcm5pY2FAc2VuYXNhZy5nb2IuYm8ifQ==.nQV9h2KlefqysEeCCwsBT+EmVfrJ2zZvpf4NtEoT3po=";
        $headers = array(
            'Content-type: application/json',
            'Authorization: '.trim($autorization),
            'Content-Length: ' . strlen($data_string),
            //'cache-control: no-cache',
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://test.senasag.gob.bo/wspaititi/api/liquidador/liquidacion/verificar");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

        //curl_setopt($ch, CURLOPT_HEADER, true);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
            exit();
        }
//        return json_decode($data);
        if ($data!='') {
            $data = json_decode($data);
            if ($data->finalizado) {
                return $data;
                }
             else {
                return response(['message' => 'No Existe liquidacion'],500);
            }
        }else{
            return response(['message' => 'No Existe liquidacion'],500);
        }

    }
//    /**
//     * Display a listing of the resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function index()
//    {
//        //
//    }
//
//    /**
//     * Show the form for creating a new resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function create()
//    {
//        //
//    }
//
//    /**
//     * Store a newly created resource in storage.
//     *
//     * @param  \App\Http\Requests\StorePedidoRequest  $request
//     * @return \Illuminate\Http\Response
//     */
//    public function store(StorePedidoRequest $request)
//    {
//        //
//         // return $request;
//        if($request->user['id']=='' || $request->user['id'] == null){
//            $user=new User();
//            $user->name=$request->user['name'];
//            $user->ci=$request->user['ci'];
//            $user->apellidos=$request->user['apellidos'];
//            $user->genero=$request->user['genero'];
//            $user->expedido=$request->user['expedido'];
//            $user->direccion=$request->user['direccion'];
//            $user->tipo=$request->user['tipo'];
//            $user->email=$request->user['email'];
//            $user->celular=$request->user['celular'];
//            $user->password=$request->user['ci'];
//            $user->save();
//        }
//        else{
//            $user=User::find($request->user['id']);
//            $user->name=$request->user['name'];
//            $user->apellidos=$request->user['apellidos'];
//            $user->genero=$request->user['genero'];
//            $user->expedido=$request->user['expedido'];
//            $user->direccion=$request->user['direccion'];
//            $user->tipo=$request->user['tipo'];
//            $user->celular=$request->user['celular'];
//            $user->save();
//
//
//        }
//
//        if($request->mascota['id']=='' || $request->mascota['id']==null){
//            $mascota=new Mascota();
//            $mascota->chip=$request->mascota['chip'];
//            $mascota->nombre=$request->mascota['nombre'];
//            $mascota->raza=$request->mascota['raza'];
//            $mascota->rasgos=$request->mascota['rasgos'];
//            $mascota->edad=$request->mascota['edad'];
//            $mascota->sexo=$request->mascota['sexo'];
//            $mascota->tipo=$request->mascota['tipo'];
//            $mascota->localidad=$request->mascota['localidad'];
//            $mascota->user_id=$user->id;
//             $mascota->save();
//        }
//        else{
//            $mascota=Mascota::find($request->mascota['id']);
//            $mascota->nombre=$request->mascota['nombre'];
//            $mascota->raza=$request->mascota['raza'];
//            $mascota->rasgos=$request->mascota['rasgos'];
//            $mascota->edad=$request->mascota['edad'];
//            $mascota->sexo=$request->mascota['sexo'];
//            $mascota->tipo=$request->mascota['tipo'];
//            $mascota->localidad=$request->mascota['localidad'];
//            $mascota->user_id=$user->id;
//            $mascota->save();
//
//    }
//    }
//
//    /**
//     * Display the specified resource.
//     *
//     * @param  \App\Models\Pedido  $pedido
//     * @return \Illuminate\Http\Response
//     */
//    public function show(Pedido $pedido)
//    {
//        //
//    }
//
//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param  \App\Models\Pedido  $pedido
//     * @return \Illuminate\Http\Response
//     */
//    public function edit(Pedido $pedido)
//    {
//        //
//    }
//
//    /**
//     * Update the specified resource in storage.
//     *
//     * @param  \App\Http\Requests\UpdatePedidoRequest  $request
//     * @param  \App\Models\Pedido  $pedido
//     * @return \Illuminate\Http\Response
//     */
//    public function update(UpdatePedidoRequest $request, Pedido $pedido)
//    {
//        //
//    }
//
//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param  \App\Models\Pedido  $pedido
//     * @return \Illuminate\Http\Response
//     */
//    public function destroy(Pedido $pedido)
//    {
//        //
//    }
}
