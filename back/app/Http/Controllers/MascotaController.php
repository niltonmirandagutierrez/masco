<?php

namespace App\Http\Controllers;

use App\Models\Mascota;
use App\Http\Requests\StoreMascotaRequest;
use App\Http\Requests\UpdateMascotaRequest;
use Illuminate\Http\Request;

class MascotaController extends Controller
{
    public function index(Request $request)
    {
        $mascotas = Mascota::all();
        return $mascotas;
    }
    public function buscarMascota($chip){
        return Mascota::where('chip',$chip)->first();
    }
    public function store(StoreMascotaRequest $request)
    {
        if($request->id!='' && $request->id!=null){
        $mascota=Mascota::find($request->id);
        $mascota->nombre=$request->nombre;
        $mascota->raza=$request->raza;
        $mascota->rasgos=$request->rasgos;
        $mascota->fechanac=$request->fechanac;
        $mascota->sexo=$request->sexo;
        $mascota->tipo=$request->tipo;
        $mascota->localidad=$request->localidad;
         $mascota->save();
        return $mascota;
        }
        else{
            $mascota=new Mascota();
        $mascota->chip=$request->chip;
        $mascota->nombre=$request->nombre;
        $mascota->raza=$request->raza;
        $mascota->rasgos=$request->rasgos;
        $mascota->fechanac=$request->fechanac;
        $mascota->sexo=$request->sexo;
        $mascota->tipo=$request->tipo;
        $mascota->localidad=$request->localidad;
        $mascota->save();
        return $mascota;
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mascota  $mascota
     * @return \Illuminate\Http\Response
     */
    public function show(Mascota $mascota)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mascota  $mascota
     * @return \Illuminate\Http\Response
     */
    public function edit(Mascota $mascota)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMascotaRequest  $request
     * @param  \App\Models\Mascota  $mascota
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMascotaRequest $request, Mascota $mascota)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mascota  $mascota
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mascota $mascota)
    {
        //
    }
}
