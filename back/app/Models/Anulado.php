<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Anulado extends Model
{
    use HasFactory;
    protected $fillable = [
        'talonario',
    'codigo',
    'fecha',
    'motivo',
    'hora',
    'tipo',
    'user_id',
    'pedido_id'
];
}
