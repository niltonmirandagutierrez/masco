<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destino extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombre',
    ];

    public function grupo(){
        return $this->belongsTo(Grupo::class)->with('requisitos');
    }
}
