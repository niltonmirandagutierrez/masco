<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombreImportador',
        'documentoImportador',
        'direccionImportador',
        'celularImportador',
        'correoImportador',
        'nombreExportador',
        'documentoExportador',
        'direccionExportador',
        'celularExportador',
        'correoExportador',
        'nroliquidacion',
        'importe',
        'fechapago',
        'codigo',
        'origen',
        'veterinario',
        'descripcion',
        'fechaRevision',
        'documento',
        'veterinario2',
        'descripcion2',
        'fechaRevision2',
        'documento2',
        'veterinario3',
        'descripcion3',
        'fechaRevision3',
        'documento3',
        'transporte',
        'lugarLlegada',
        'puertoSalida',
        'estado',
        'inicio',
        'emision',
        'validez',
        'observacion',
        'verificacion',
        'destino_id',
        'user_id',
        'mascota_id',
        'senasag_id',
    ];

    public function mascota(){
        return $this->belongsTo(Mascota::class);
    }

    public function destino(){
        return $this->belongsTo(Destino::class)->with('grupo');
    }

    public function pagos(){
        return $this->hasMany(Pago::class);
    }
}
