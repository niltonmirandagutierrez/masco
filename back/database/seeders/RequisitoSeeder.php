<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RequisitoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('requisitos')->insert([
            [
                "nombre"=>"1.	Certificado de Vacunación Antirrábica en vigencia extendido por un Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA.",
                "grupo_id"=>1,
            ],
            [
                "nombre"=>"2.	Certificado de Salud en ejemplar original extendido por Profesional Veterinario Privado,en el modelo provisto por el Consejo o Colegio de la Jurisdicción,dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>1,
            ],
            [
                "nombre"=>"3.	Documento de Identidad de la persona que trasladará al animal al país de grupo.",
                "grupo_id"=>1
            ],
            [
                "nombre"=>"4.	Para Caninos:Certificado vacuna sextuple:Distemper, Adenovirus, Hepatitis canina, Leptospirosis, Parvovirosis y Parainfluenza, con Coronavirus canino.",
                "grupo_id"=>1
            ],
            [
                "nombre"=>"5.	Para Felinos: Analisis de dermatofitosis para felinos.",
                "grupo_id"=>1
            ],
            [
                "nombre"=>"6.	Para Felinos: Vacuna contra Panleucopenia felina.",
                "grupo_id"=>1
            ],
            [
                "nombre"=>"7.	Animales menores a 3 meses: no se le pide el punto 1.",
                "grupo_id"=>1
            ],
            [
                "nombre"=>"1.	Certificado de Vacunación Antirrábica en vigencia extendido por un Profesional Veterinario Privado en el modelo provisto por el Consejo o Colegio de la jurisdiccion, con vacuna aprobada por SENASA.",
                "grupo_id"=>2
            ],
            [
                "nombre"=>"2.	La fecha de aplicación de la vacunación antirrábica debe ser posterior a la fecha de implantación del microchip sin excepción además  la vacuna antirrábica será considerada válida a partir de los veintiún (21) días de la fecha de su administración. Podrán eximirse de este lapso aquellas vacunaciones de refuerzo contra la rabia  administradas al animal durante el período de validez de una vacuna antirrábica anterior, de la que pueda darse debida fe documental.",
                "grupo_id"=>2
            ],
            [
                "nombre"=>"3.	Certificado de Salud  en ejemplar original, extendido por Profesional Veterinario Privado ,en el modelo provisto por el Consejo o Colegio de la jurisdicción , dentro  de los DIEZ (10) días de la fecha de embarque.",
                "grupo_id"=>2
            ],
            [
                "nombre"=>"4.	DNI /pasaporte de la persona que trasladará el animal hasta el país de grupo o, en caso que el animal viajara “no acompañado”, una fotocopia del Documento de Identidad del Propietario/Responsable del animal en la República Argentina o en el país de grupo, a cuyo nombre será extendido el Certificado Veterinario Internacional.",
                "grupo_id"=>2
            ],
            [
                "nombre"=>"5.	Presentar una fotocopia del comprobante de la aplicación del microchip realizado por Profesional Veterinario matriculado.",
                "grupo_id"=>2
            ],
            [
                "nombre"=>"6.	No se acepta el ingreso de animales menores de tres (3) meses.",
                "grupo_id"=>2
            ],
            [
                "nombre"=>"1.	Certificado de Vacunación Antirrábica en vigencia extendido por un Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA.",
                "grupo_id"=>3
            ],
            [
                "nombre"=>"2.	Certificado de Salud en ejemplar original extendido por Profesional Veterinario Privado,en el modelo provisto por el Consejo o Colegio de la Jurisdicción,dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>3
            ],            
            [
                "nombre"=>"3.	Documento de Identidad de la persona que trasladará al animal al país de grupo.",
                "grupo_id"=>3
            ],            
            [
                "nombre"=>"4.	Certificado vacuna séxtuple caninos (Distemper, Adenovirus, Hepatitis canina, Leptospirosis, Parvovirosis y Parainfluenza) con Coronavirus canino.",
                "grupo_id"=>3
            ],            
            [
                "nombre"=>"5.	Para felinos certificado de vacunación contra Panleucopenia felina.",
                "grupo_id"=>3
            ],           
            [
                "nombre"=>"6.	Analisis de dermatofitosis para felinos.",
                "grupo_id"=>3
            ],
            [
                "nombre"=>"7.	Animales menores a 3 meses: no se le pide el punto 1.",
                "grupo_id"=>3
            ],
            [
                "nombre"=>"1.	Concurrencia con el animal para la constatación de su identificación por lectura oficial del microchip o del tatuaje o  presentar Certificado de Lectura de Microchip emitido por el Veterinario privado matriculado donde consten los datos del animal, propietario y el número del dispositivo electrónico (microchip).",
                "grupo_id"=>4
            ],
            [
                "nombre"=>"2.	Certificado de Vacunación Antirrábica en vigencia,extendido por un Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA.La fecha de aplicación de la vacunación antirrábica debe ser posterior a la fecha de implantación del microchip sin excepción además  la vacuna antirrábica será considerada válida a partir de los veintiún (21) días de la fecha de su administración. Podrán eximirse de este lapso aquellas vacunaciones de refuerzo contra la rabia  administradas al animal durante el período de validez de una vacuna antirrábica anterior, de la que pueda darse debida fe documental.",
                "grupo_id"=>4
            ],
            [
                "nombre"=>"3.	Certificado de Salud  en ejemplar original, extendido por Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la jurisdicción, dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>4
            ],
            [
                "nombre"=>"4.	Fotocopia de Documento de identidad de la persona que trasladará el animal hasta el país de grupo o, en caso que el animal viajara 'no acompañado', una fotocopia del Documento de identidad del propietario/responsable del animal en la REPUBLICA ARGENTINA y en el país de grupo, a cuyo nombre será extendido el Certificado Veterinario Internacional.",
                "grupo_id"=>4
            ],
            [
                "nombre"=>"5.	Presentar una fotocopia del comprobante de la aplicación del microchip realizado por Profesional Veterinario matriculado.",
                "grupo_id"=>4
            ],
            [
                "nombre"=>"6.	No se acepta el ingreso de menores de tres meses.",
                "grupo_id"=>4
            ],
            [
                "nombre"=>"1.	Certificado de Vacunación Antirrábica en vigencia extendido por un Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la jurisdicción, con vacuna aprobada por SENASA.",
                "grupo_id"=>5
            ],
            [
                "nombre"=>"2.	Certificado de Salud en ejemplar original extendido por Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la jurisdicción, dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>5
            ],
            [
                "nombre"=>"3.	Documento de Identidad de la persona que viajara con el animal.",
                "grupo_id"=>5
            ],
            [
                "nombre"=>"4.	Animales menores a 3 meses: no se le pide el punto 1.",
                "grupo_id"=>5
            ],
            [
                "nombre"=>"1.	Certificado de Vacunación Antirrábica en vigencia extendido por un Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA.",
                "grupo_id"=>6
            ],
            [
                "nombre"=>"2.	Certificado de Salud en ejemplar original extendido por Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>6
            ],
            [
                "nombre"=>"3.	Documento de Identidad de la persona que viajara con el animal",
                "grupo_id"=>6
            ],
            [
                "nombre"=>"4.	Comprobante de la aplicación del microchip realizado por profesional veterinario matriculado.",
                "grupo_id"=>6
            ],
            [
                "nombre"=>"1.	Certificado de Vacunación Antirrábica en vigencia extendido por un Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la jurisdicción, con vacuna aprobada por SENASA, debe aplicarse dentro de un año y no menos de un mes.",
                "grupo_id"=>7
            ],
            [
                "nombre"=>"2.	Certificado de vacunación en perros: moquillo, parvovirus,hepatitis, leptospirosis; gatos: panleucopenia, rinotraqueitis y calicivirosis.",
                "grupo_id"=>7
            ],
            [
                "nombre"=>"3.	Certificado de Salud en ejemplar original extendido por Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>7
            ],
            [
                "nombre"=>"4.	Documento de Identidad de la persona que viajará con el animal.",
                "grupo_id"=>7
            ],
            [
                "nombre"=>"1.	Certificado de Vacunación Antirrábica en vigencia,extendido por un Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA.La fecha de aplicación de la vacunación antirrábica debe ser posterior a la fecha de implantación del microchip sin excepción además  la vacuna antirrábica será considerada válida a partir de los veintiún (21) días de la fecha de su administración. Podrán eximirse de este lapso aquellas vacunaciones de refuerzo contra la rabia  administradas al animal durante el período de validez de una vacuna antirrábica anterior, de la que pueda darse debida fe documental.",
                "grupo_id"=>8
            ],
            [
                "nombre"=>"2.	Certificado de Salud  en ejemplar original, extendido por Profesional Veterinario Privado,en el modelo provisto por el Consejo o Colegio de la Jurisdicción, dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>8
            ],
            [
                "nombre"=>"3.	Fotocopia de Documento de identidad de la persona que trasladará el animal hasta el país de grupo o, en caso que el animal viajara 'no acompañado', una fotocopia del Documento de identidad del propietario/responsable del animal en la REPUBLICA ARGENTINA y en el país de grupo, a cuyo nombre será extendido el Certificado Veterinario Internacional.",
                "grupo_id"=>8
            ],
            [
                "nombre"=>"4.	Certificado de desparasitación interna y externa dentro de los 15 (quince) días previos a la emisión del Certificado Veterinario Internacional, indicando: laboratorio, marca comercial, principio activo y fecha de administración.",
                "grupo_id"=>8
            ],
            [
                "nombre"=>"1.	Certificado de Salud, extendido por Profesional Veterinario Privado en el modelo provisto por el Consejo o Colegio de la Jurisdicción,dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>9
            ],
            [
                "nombre"=>"2.	Presentación de ejemplar original y una fotocopia del Certificado de Vacunación Antirrábica,en vigencia extendido por un Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA  habiendo sido vacunado en el período no menor a treinta (30) días y pero no mayor a un (1) año, en el caso de una primo vacunación.",
                "grupo_id"=>9
            ],
            [
                "nombre"=>"3.	Presentación de ejemplares originales y una fotocopia de 2 Certificados de Vacunación Antirrábica, habiendo sido revacunado contra la rabia en un período no menor a 30 días pero no mayor a 3 años en el caso de perros. En el caso de gatos, el período entre cada vacunación deberá estar comprendido entre los treinta (30) días y un (1) año.",
                "grupo_id"=>9
            ],
            [
                "nombre"=>"En el caso de animales menores de 3 meses, el usuario deberá presentar el Certificado de Vacunación Antirrábica de la madre en el cual conste que la misma fue vacunada al menos 30 días antes pero no más de 12 meses previos al parto.",
                "grupo_id"=>9
            ],
            [
                "nombre"=>"1.	Presentación de Certificado de salud original, extendido por el profesional veterinario matriculado, en el modelo provisto por el Consejo o Colegio de la jurisdicción,  donde conste que el animal se encuentra libre de enfermedades infecciosas y parásitos externos.",
                "grupo_id"=>10
            ],
            [
                "nombre"=>"2.	Dicho certificado tendrá una validez de diez (10) días dentro  de la fecha de embarque.",
                "grupo_id"=>10
            ],
            [
                "nombre"=>"3.	Presentación de ejemplar original y una fotocopia del Certificado de Vacunación Antirrábica,en vigencia extendido por un Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la jurisdicción, con vacuna aprobada por SENASA,habiendo sido vacunado en el período no menor a treinta (30) días y pero no mayor a once (11) meses antes de la fecha de embarque.",
                "grupo_id"=>10
            ],
            [
                "nombre"=>"Presentación de una constancia que indique el animal tiene al menos 4 meses de edad",
                "grupo_id"=>10
            ],
            [
                "nombre"=>"1.	Certificado de Vacunación Antirrábica en vigencia extendido por un Profesional Veterinario Privado,en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA.",
                "grupo_id"=>11
            ],
            [
                "nombre"=>"2.	Certificado de Salud en ejemplar original extendido por Profesional Veterinario Privado,en el modelo provisto por el Consejo o Colegio de la Jurisdicción, dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>11
            ],
            [
                "nombre"=>"3.	Documento de Identidad de la persona que viajará con el animal.",
                "grupo_id"=>11
            ],
            [
                "nombre"=>"4.	Animales menores a 3 meses: no se le pide el punto 1.",
                "grupo_id"=>11
            ],
            [
                "nombre"=>"1.	Concurrencia con el animal para la constatación de su identificación por lectura oficial del microchip o del tatuaje o  presentar Certificado emitido por el Veterinario privado matriculado donde consten los datos del animal, propietario y el número del dispositivo electrónico (microchip).",
                "grupo_id"=>12
            ],
            [
                "nombre"=>"2.	Certificado de Vacunación Antirrábica en vigencia extendido por un Profesional Veterinario Privado.en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA.",
                "grupo_id"=>12
            ],
            [
                "nombre"=>"3.	Titulación de anticuerpos antirabicos: en el lapso mínimo de VEINTIUN (21) días posteriores a la vacunación antirrábica, el propietario deberá enviar una muestra serológica del animal - obtenida por profesional veterinario privado - para la titulación de Anticuerpos Antirrábicos a un laboratorio oficial de diagnóstico reconocido por el país de grupo del animal.",
                "grupo_id"=>12
            ],
            [
                "nombre"=>"4.	Certificado de Salud en ejemplar original extendido por Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción,dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>12
            ],
            [
                "nombre"=>"5.	Tratado contra parásitos internos y externos dentro de los SEIS (6) meses previos al embarque.",
                "grupo_id"=>12
            ],
            [
                "nombre"=>"6.	Constancia de la identificación del animal El propietario / responsable del animal deberá presentar el ejemplar original y una copia de la Constancia de la identificación del animal por microchips efectuada por profesional veterinario matriculado. ",
                "grupo_id"=>12
            ],
            [
                "nombre"=>"7.	Documento de Identidad",
                "grupo_id"=>12
            ],
            [
                "nombre"=>"1.	Certificado de Vacunación Antirrábica, en vigencia , en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA,  extendido por un Profesional Veterinario Privado. El  original de este Certificado será devuelto al usuario luego de realizarse las constataciones de rigor. Cuando esta vacunación hubiera sido realizada en el exterior del país, deberá presentarse la constancia correspondiente en original.",
                "grupo_id"=>13
            ],
            [
                "nombre"=>"2.	Certificado de Salud en ejemplar original extendido por Profesional Veterinario Privado,en el modelo provisto por el Consejo o Colegio de la Jurisdicción, dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>13
            ],
            [
                "nombre"=>"3.	Documento de Identidadde la persona que trasladará al animal al país de grupo.",
                "grupo_id"=>13
            ],
            [
                "nombre"=>"4.	Animales menores a 3 meses: NO SE ADMITEN",
                "grupo_id"=>13
            ],
            [
                "nombre"=>"5.	REQUIERE PERMISO DE IMPORTACION DEL PAIS grupo",
                "grupo_id"=>13
            ],
            [
                "nombre"=>"6.	CANINOS: LEPTOSPIROSIS, CORONAVIROSIS, PARVOVIROSIS, PARAINFLUENZA, MOQUILLO, HEPATITIS INFECCIOSA",
                "grupo_id"=>13
            ],
            [
                "nombre"=>"7.	FELINOS: PANLEUCOPENIA",
                "grupo_id"=>13
            ],
            [
                "nombre"=>"1.	CERTIFICADO DE IMPLANTACION DE MICROCHIP NORMA ISO 11.784, 11.785 (ORIGINAL Y COPIA)",
                "grupo_id"=>14
            ],
            [
                "nombre"=>"2.	VACUANCION ANTIRRABICA EN CERTIFICADO OFICIAL EN VIGENCIA EXTENDIDO POR UN PROFESIONAL VETERINARIO PRIVADO, EN EL MODELO PROVISTO POR EL CONSEJO O COLEGIO DE LA JURISDICCIÓN, CON VACUNA APROBADA POR SENASA,CON 21 DIAS DE APLICADA (ORIGINAL Y COPIA)",
                "grupo_id"=>14
            ],
            [
                "nombre"=>"3.	CANINOS: VACUNA SEXTUPLE + CORONAVIRUS + LEPTOSPIROSIS, FELINOS: VACUNA TRIPLE FELINA: RINOTRACHEITIS FELINA, CALICIVIRUS FELINO Y PANLEUCOPENIA FELINA.  (ORIGINAL Y COPIA)",
                "grupo_id"=>14
            ],
            [
                "nombre"=>"4.	CERTIFICADO DE SALUD EN EJEMPLAR ORIGINAL EXTENDIDO POR PROFESIONAL VETERINARIO PRIVADO EN EL MODELO PROVISTO POR EL CONSEJO O COLEGIO DE LA JURISDICCIÓN, DENTRO  DE LOS DIEZ (10) DÍAS ANTERIORES A LA FECHA DE EMBARQUE.",
                "grupo_id"=>14
            ],
            [
                "nombre"=>"5.	CONCURRIR CON EL ANIMAL A ESTA  OFICINA UNA SEMANA  ANTES DEL VIAJE.",
                "grupo_id"=>14
            ],
            [
                "nombre"=>"6.	FOTOCOPIA DEL PASAPORTE DE LA PERSONA QUE VIAJARA CON EL ANIMAL. Y FOTOCOPIA DE LA CONSTANCIA  DE CUIT",
                "grupo_id"=>14
            ],
            [
                "nombre"=>"Concurrencia con el animal para la constatación de su identificación por lectura oficial del microchip o del tatuaje o presentar Certificado de Lectura de Microchip emitido por el Veterinario privado matriculado donde consten los datos del animal, propietario y el número del dispositivo electrónico (microchip).",
                "grupo_id"=>14
            ],
            [
                "nombre"=>"1.	Certificado de Vacunación Antirrábica en vigencia extendido por un Profesional Veterinario Privado,extendido por un Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA.",
                "grupo_id"=>15
            ],
            [
                "nombre"=>"2.	Certificado de Salud en ejemplar original extendido por Profesional Veterinario Privado,en el modelo provisto por el Consejo o Colegio de la Jurisdicción, dentro  de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>15
            ],
            [
                "nombre"=>"3.	Documento de Identidad de la persona que viajará con el animal.",
                "grupo_id"=>15
            ],
            [
                "nombre"=>"4.	Animales menores a 3 meses: no se le pide el punto 1.",
                "grupo_id"=>15
            ],
            [
                "nombre"=>"1.	Certificado de Vacunación Antirrábica, en vigencia, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA, extendido por un Profesional Veterinario Privado. El original de este Certificado será devuelto al usuario luego de realizarse las constataciones de rigor. Cuando esta vacunación hubiera sido realizada en el exterior del país, deberá presentarse la constancia correspondiente en original.",
                "grupo_id"=>16
            ],
            [
                "nombre"=>"2.	Certificado de Salud en ejemplar original extendido por Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, dentro de los DIEZ (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>16
            ],
            [
                "nombre"=>"3.	Documento de Identidad de la persona que trasladará al animal al país de grupo.",
                "grupo_id"=>16
            ],
            [
                "nombre"=>"4.	Animales menores a 3 meses: no se le pide el punto 1.",
                "grupo_id"=>16
            ],
            [
                "nombre"=>"2.	Dicho certificado se deberá tramitar dentro de los  cinco (5) días anteriores a la fecha de embarque.",
                "grupo_id"=>17
            ],
            [
                "nombre"=>"3.	Presentación de ejemplar original y una fotocopia del Certificado de Vacunación Antirrábica, en vigencia extendido por un Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la Jurisdicción, con vacuna aprobada por SENASA  y contra las siguientes enfermedades: DIstemper, Hepatitis canina, Leptospirosis,  y Parvovirosis, habiendo sido vacunado en el período no menor a veintiún (21) días antes de la exportación.",
                "grupo_id"=>17
            ],
            [
                "nombre"=>"1.	Certificado de implantación de microchip norma ISO 11.784, 11.785 (original y copia)",
                "grupo_id"=>18
            ],
            [
                "nombre"=>"2.	Vacunación antirrábica en certificado oficial con 21 días de aplicada (original y copia), en vigencia extendido por un profesional veterinario privado, en el modelo provisto por el consejo o colegio de la jurisdicción, con vacuna aprobada por el Senasa.",
                "grupo_id"=>18
            ],
            [
                "nombre"=>"3.	Caninos: vacuna séxtuple + coronavirus + leptospirosis,",
                "grupo_id"=>18
            ],
            [
                "nombre"=>"4.	felinos: vacuna triple felina: rinotracheitis felina, calicivirus felino y panleucopenia felina.  (original y copia)",
                "grupo_id"=>18
            ],
            [
                "nombre"=>"5.	Certificado de salud, extendido por profesional veterinario privado en el modelo provisto por el consejo o colegio de la jurisdicción dentro  de los diez (10) días anteriores a la fecha de embarque.",
                "grupo_id"=>18
            ],
            [
                "nombre"=>"6.	Certificado de desparasitación externa e interna",
                "grupo_id"=>18
            ],
            [
                "nombre"=>"Concurrencia con el animal para la constatación de su identificación por lectura oficial del microchip o del tatuaje o presentar Certificado de Lectura de Microchip emitido por el Veterinario privado matriculado donde consten los datos del animal, propietario y el número del dispositivo electrónico (microchip).",
                "grupo_id"=>18
            ],
            [
                "nombre"=>"1.	Permiso de importación emitido por la autoridad sanitaria de Qatar, disponible en el siguiente enlace:https://www.mme.gov.qa/cui/view.dox?id=1535&sitid=1",
                "grupo_id"=>19
            ],
            [
                "nombre"=>"2.	Certificado de Implantación del microchip y/o Lectura de microchip realizado por Profesional Veterinario matriculado donde consten los datos del animal, propietario, fecha de implantación/lectura y el número del dispositivo electrónico (microchip).",
                "grupo_id"=>19
            ],
            [
                "nombre"=>"3.	Certificado de Vacunación Antirrábica en vigencia extendido por un Profesional Veterinario Privado en el modelo provisto por el Consejo o Colegio de la jurisdicción, con vacuna aprobada por SENASA. Los animales mayores a los TRES (3) meses de edad deben estar vacunados  contra la rabia, y deben trascurrir al menos 30 (treinta) días luego de aplicada dicha vacunación, antes de la exportación al Estado de Qatar.",
                "grupo_id"=>19
            ],
            [
                "nombre"=>"4.	Titulación de anticuerpos Antirrábicos, realizado en un laboratorio acreditado por Senasa.",
                "grupo_id"=>19
            ],
            [
                "nombre"=>"5.	Certificado de vacunación séxtuple (PERROS): Vacunación contra la Enfermedad de Carré (Distemper canino), Hepatitis canina, Coronavirus, Leptospirosis (L canícola y L. icterohemorragiae), Parvovirosis (parvovirus canino) y Parainfluenza utilizando vacunas aprobadas por el SENASA, llevada a cabo por profesional veterinario privado con matriculación.",
                "grupo_id"=>19
            ],
            [
                "nombre"=>"6.	Certificado de vacunación triple felina (GATOS) Vacunación contra Rinotraqueitis, Panleucopenia y Calicivirosis utilizando vacunas aprobadas por el SENASA, llevada a cabo por profesional veterinario privado con matriculación.",
                "grupo_id"=>19
            ],
            [
                "nombre"=>"7.	Certificado de Salud en ejemplar original, extendido por Profesional Veterinario Privado, en el modelo provisto por el Consejo o Colegio de la jurisdicción, dentro de los DIEZ (10) días a la fecha de embarque.",
                "grupo_id"=>19
            ],
            [
                "nombre"=>"8.	DNI /pasaporte de la persona que trasladará el animal hasta el país de grupo o, en caso que el animal viajara “no acompañado”, una fotocopia del Documento de Identidad del Propietario/Responsable del animal en el país de grupo, a cuyo nombre será extendido el Certificado Veterinario Internacional.",
                "grupo_id"=>19
            ],
            [
                "nombre"=>"8.	DNI /pasaporte de la persona que trasladará el animal hasta el país de grupo o, en caso que el animal viajara “no acompañado”, una fotocopia del Documento de Identidad del Propietario/Responsable del animal en el país de grupo, a cuyo nombre será extendido el Certificado Veterinario Internacional.",
                "grupo_id"=>20
            ],
        ]);
    }
}
