<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DestinoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('destinos')->insert([
            [
                'id'=>1,
                'nombre' => 'KAZAJISTAN',
                'grupo_id' => 1,
            ],
            [
                'id'=>2,
                'nombre' => 'BIELORRUSIA',
                'grupo_id' => 1,
            ],
            [
                'id'=>3,
                'nombre' => 'KIRGUISTAN',
                'grupo_id' => 1,
            ],
            [
                'id'=>4,
                'nombre' => 'RUSIA',
                'grupo_id' => 1,
            ],
            [
                'id'=>5,
                'nombre' => 'ARMENIA',
                'grupo_id' => 1,
            ],

        ]);
    }
}
