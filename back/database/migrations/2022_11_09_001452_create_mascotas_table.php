<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMascotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mascotas', function (Blueprint $table) {
            $table->id();
            $table->string('chip');
            $table->string('nombre');
            $table->string('raza');
            $table->string('rasgos');
            //$table->string('edad');
            $table->date('fechanac');
            $table->string('sexo');
            $table->string('tipo');
//            $table->string('foto');
            $table->string('localidad');
//            $table->unsignedBigInteger('user_id');
 //           $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mascotas');
    }
}
