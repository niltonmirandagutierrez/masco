<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnuladosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anulados', function (Blueprint $table) {
            $table->id();
            $table->string('talonario');
            $table->string('motivo');
            $table->string('codigo');
            $table->string('tipo');
            $table->date('fecha');
            $table->time('hora');
            $table->integer('pedido_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anulados');
    }
}
