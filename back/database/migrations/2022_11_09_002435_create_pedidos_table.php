<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->string('nombreImportador'); //
            $table->string('documentoImportador')->nullable();
            $table->string('direccionImportador');
            $table->string('celularImportador');
            $table->string('correoImportador');

            $table->string('nombreExportador'); //
            $table->string('documentoExportador');
            $table->string('direccionExportador');
            $table->string('celularExportador');
            $table->string('correoExportador');
            $table->string('nroliquidacion'); // dato de la api
            $table->double('importe'); // dato de la api
            $table->date('fechapago'); // dato de la api
            $table->integer('codigo')->nullable();  //codigo departamento de tramite
            $table->integer('talonario')->nullable();  //codigo nacional de tramite

            $table->string('origen');  // departamento de tramite
                //revision medica
            $table->string('veterinario');// nombre doc autorizand
            $table->string('descripcion');// descrp de la prueba
            $table->date('fechaRevision');// fecha de la prueba
            $table->string('documento');
            // serologia
            $table->string('veterinario2')->nullable();// nombre doc autorizand
            $table->string('descripcion2')->nullable();// descrp de la prueba
            $table->date('fechaRevision2')->nullable();// fecha de la prueba
            $table->string('documento2')->nullable( );
            //antirrabico
            $table->string('veterinario3');// nombre doc autorizand
            $table->string('descripcion3');// descrp de la prueba
            $table->date('fechaRevision3');// fecha de la prueba
            $table->string('documento3');

           // $table->string('importador');// importador o dueño

            $table->string('transporte');//aereo o terrestre o maritimo multimodal
            $table->string('lugarLlegada'); // lugar de destino
            $table->string('puertoSalida'); // lugar de sald

            $table->string('estado')->default('pendiente'); // modicar  finalizado
// fecha inicio
            $table->date('inicio');
//fecha emision
            $table->date('emision')->nullable();
// fecha valido + devefcto 30 dias o 60 dias
            $table->date('validez')->nullable();
            $table->string('observacion')->nullable();
            $table->string('verificacion')->default('pendiente');// aceptado observado,
            $table->unsignedBigInteger('destino_id');
            $table->foreign('destino_id')->references('id')->on('destinos');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('mascota_id')->nullable();
            $table->foreign('mascota_id')->references('id')->on('mascotas');
            $table->unsignedBigInteger('senasag_id')->nullable();
            $table->foreign('senasag_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
