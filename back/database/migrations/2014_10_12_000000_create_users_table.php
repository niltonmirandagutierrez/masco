<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->default('');
            $table->string('ci')->unique();
            $table->string('apellidos')->default('');
//            $table->string('apellidoMaterno')->default('');
            $table->string('genero')->default('');
            $table->string('expedido')->default('');
            $table->string('direccion')->default('');
            $table->string('tipo')->default('persona');
            $table->string('email')->nullable()->default('');
            $table->string('ciudad')->nullable()->default('');
            $table->string('celular')->nullable()->default('');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable()->default('');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
